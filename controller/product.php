<?php

class productController {

    public function index() {
        require 'view/show.php';
    }

    public function Create() {
        require 'view/create.php';
    }

    /*     * <p> Function deleteUsingCheckbox() is used to delete products and then we pass all those
     * product to the model of Product.</p>
      @param array $products <p>Is a array where we have a product sku</p>. */

    static function deleteUsingCheckbox($products) {
        require 'view/show.php';
        require 'model/Product.php';
        Product::deleteProductChecked($products);
        echo '<meta http-equiv="refresh" content="1; url=index.php">';
    }

}
