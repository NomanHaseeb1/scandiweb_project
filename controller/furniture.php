<?php

require 'product.php';

class furnitureController extends productController {
    /* Add a Product in this case it is the furniture in the database */
    public function add() {
        require_once 'model/Furniture.php';
        $sku = $_POST['sku'];
        $letter = str_split($sku);
        if ($letter[0] != 'F' && isset($_POST['sku']) && isset($_POST['name']) && isset($_POST['price']) && isset($_POST['hieght']) && isset($_POST['width']) && isset($_POST['length'])) {
            require_once 'view/Error/Error2.php';
        } else {
            $furniture = new Furniture($_POST['sku'], $_POST['name'], $_POST['price'], $_POST['hieght'], $_POST['width'], $_POST['length']);
            if ($furniture->checkSku($_POST['sku'])) {
                $furniture->insert($furniture);
                header('Location: ../index.php');
            } else {
                require_once 'view/Error/Error.php';
            }
        }
    }

}
