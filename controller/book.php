<?php

require 'product.php';

class bookController extends productController {
    /* Function which is used to add a product in this case a book into a database */
    public function add() {
        require_once 'model/Book.php';
        $sku = $_POST['sku'];
        $letter = str_split($sku);
        if ($letter[0] != 'B' && isset($_POST['sku']) && isset($_POST['name']) && isset($_POST['price']) && isset($_POST['weight'])) {
            require_once 'view/Error/Error2.php';
        } else {
            $book = new Book($_POST['sku'], $_POST['name'], $_POST['price'], $_POST['weight']);
            if ($book->checkSku($_POST['sku'])) {
                $book->insert($book);
                header('Location: ../index.php');
            } else {
                require_once 'view/Error/Error.php';
            }
        }
    }

}
