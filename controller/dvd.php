<?php
require 'product.php';
class dvdController extends productController{
    /*Function used to add a dvd into the database by creating a dvd object*/
     public function add() {
        require_once 'model/Dvd.php';
        $sku = $_POST['sku'];
        $letter = str_split($sku);
        if ($letter[0] != 'D' && isset($_POST['sku']) && isset($_POST['name']) && isset($_POST['price']) && isset($_POST['size'])) {
            require_once 'view/Error/Error2.php';
        } else {
            $dvd = new Dvd($_POST['sku'], $_POST['name'], $_POST['price'], $_POST['size']);
            if ($dvd->checkSku($_POST['sku'])) {
                $dvd->insert($dvd);
                header('Location: ../index.php');
            } else {
                require_once 'view/Error/Error.php';
            }
        }
    }
    
}

