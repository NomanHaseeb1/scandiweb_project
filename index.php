<?php

require_once 'model/database.php';
$controller = 'product';
// This is the logic which will act as a front controller
if (!isset($_REQUEST['c'])) {
    require_once "controller/$controller.php";
    $controller = ucwords($controller) . 'Controller';
    $controller = new $controller;
    /* Check if the selector-form is set than we are going to add a product otherwise we are going to display */
    if (isset($_POST['selector-form'])) {
        $controller->Create();
    } else {
        $controller->Index();
    }
} else {
    // Using this else part we are pointing to the function which comes from get url
    $controller = strtolower($_REQUEST['c']);
    $accion = isset($_REQUEST['a']) ? $_REQUEST['a'] : 'Index';

    // We are going to make a instance object
    require_once "controller/$controller.php";
    $controller = ucwords($controller) . 'Controller';
    $controller = new $controller;

    // Using this function Iam going to call the function
    call_user_func(array($controller, $accion));
}
    