<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>List Products</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
        <!-- Links for boostrap but using jquery-->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
    </head>
    <body>
        <!-- Headers & Footers page-->
        <?php
        include_once 'header-footer/header.php';
        ?>
        <div class="container">
            <div class="row">
                <?php
                require 'model/Furniture.php';
                foreach (Furniture::List_Furnitures() as $r) :
                    ?>
                    <div class="col-sm-4">
                        <div class="card text-white bg-secondary mb-3" style="max-width: 18rem;">
                            <div class="card-body">
                                <h5 class="card-title"><b><?php echo $r->sku ?></b></h5>
                                <p class="card-text"><b><?php echo $r->name ?></b></p>
                                <p class="card-text"><?php echo 'Price ' . $r->price . '$' ?></p>
                                <p class="card-text"><?php echo 'Dimesiones: ' . $r->hieght . 'x' . $r->width . 'x' . $r->length ?></p>
                                
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>

                <?php
                require 'model/Dvd.php';
                foreach (Dvd::List_Dvd() as $r) :
                    ?>
                    <div class="col-sm-4">
                        <div class="card text-white bg-secondary mb-3" style="max-width: 18rem;">
                            <div class="card-body">
                                <h5 class="card-title"><b><?php echo $r->sku ?></b></h5>
                                <p class="card-text"><b><?php echo $r->name ?></b></p>
                                <p class="card-text"><?php echo 'Price ' . $r->price . '$' ?></p>
                                <p class="card-text"><?php echo 'Size ' . $r->size . 'MB' ?></p>
                                
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
                <?php
                require 'model/Book.php';
                foreach (Book::List_Book() as $r) :
                    ?>
                    <div class="col-sm-4">
                        <div class="card text-white bg-secondary mb-3" style="max-width: 18rem;">
                            <div class="card-body">
                                <h5 class="card-title"><b><?php echo $r->sku ?></b></h5>
                                <p class="card-text"><b><?php echo $r->name ?></b></p>
                                <p class="card-text"><?php echo 'Price ' . $r->price . '$' ?></p>
                                <p class="card-text"><?php echo 'Weight: ' . $r->weight . 'Kg' ?></p>
                                
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>


                </body>
                </html>

