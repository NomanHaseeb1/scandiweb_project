<!-- This is the container -->
<div class="container">
    <!-- Inside a row there are 2 columns -->
    <div class="row">
        <div class="col-8">
            <h1>Product List</h1>
        </div>
        <!-- Delete using the Mass data Action in dropdown-->
        <div class="col-3">
            <form method="POST" action="index.php">
                <Select class="browser-default custom-select mt-2" name="selector-form" required>
                    <option value="">None</option>
                    <option value="Add">Add a Product</option>
                </select>
        </div>
        <div class="col-14">
            <button type="submit" class="btn btn-primary mt-2">Apply</button> 
        </div>
        </form>
    </div>
</div>

<hr>

