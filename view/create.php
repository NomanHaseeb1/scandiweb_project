<html>
    <head>
        <meta charset="UTF-8">
        <title>Add new Product</title>
        <!-- Links for boostrap-->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
        <!-- Links for boostrap but using jquery-->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="view/js/javascript.js"></script>
    </head>
    <body>
        <form id="myform" name="myform" method="POST" action =" ">
            <div class="row">
                <div class="col ml-5">
                    <h1>Add Product</h1>
                </div>
                <div class="col-6">
                </div>
                <div class="col">
                    <button type="submit" class="btn btn-primary mt-2" name="insert" value="insert">Apply</button>
                </div>
            </div>
            <hr>
            <div class="container mt-5">
                <div class="form-group">
                    <label for="SKU">SKU</label>
                    <input name="sku" type="" class="form-control" placeholder="SKU" pattern="[BDF]{1}[0-9]{8}"  title="B->Book  F->Dvd  F->Furniture ,the format will be: BXXXXXXX1 8 are the numbers." required="">
                    <label for="Name">Name</label>
                    <input name="name" type="text" class="form-control" placeholder="Name" required="">
                    <label for="SKU">Price</label>
                    <input name="price" class="form-control" placeholder="price" title="The price can be in int 90 or 90.01." pattern="[0-9]*[.]?[0-9]{2}" required="">
                </div>
                <div class="form-group">
                    <label>Select a product</label>
                    <Select class="browser-default custom-select" name="selector-form" onchange='changeAction(this.value)' id="form-selector" required>
                        <option value="">None</option>
                        <option value="furniture">furniture</option>
                        <option value="dvd">dvd</option>
                        <option value="book">book</option>
                    </Select>
                    <div id="furniture" class="forms" style="display:none">
                        <label for="Hieght">Hieght</label>
                        <input name="hieght" pattern="[0-9]{1,10}" maxlength="10" type="" class="form-control" placeholder="Hieght">
                        <label for="Width">Width</label>
                        <input name="width" pattern="[0-9]{1,10}" maxlength="10" minlength="0" type="" class="form-control" placeholder="Width">
                        <label for="SKU">Length</label>
                        <input name="length" pattern="[0-9]{1,10}"  maxlength="10" minlength="0" type="" class="form-control" placeholder="Length">
                        <p><i>Furniture Dimensiones--></i> will be writed in meters for example 45x55x40</p>
                    </div>
                    <div id="dvd" class="forms" style="display:none"> 
                        <label for="Size">Size</label>
                        <input type="text" class="form-control" name="size" placeholder="Size">
                        <p><i>Dvd Size--></i> will be writed in MegaBytes(MB).</p>
                    </div>
                    <div id="book" class="forms" style="display:none">
                        <label for="Weight">Weight</label>
                        <input type="text" class="form-control" name="weight" placeholder="Weight">
                        <p><i>Book Size--></i> will be writed in Kilograms(Kg).</p>
                    </div>
                </div>
            </div>
    </body>
</html>


