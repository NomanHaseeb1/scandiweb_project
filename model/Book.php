<?php

require_once 'Product.php';

class Book extends Product {
    /* Atributes */

    private $weight;

    /*     * <p>Constructor used to create a Book.</p> */

    public function __construct($sku, $name, $price, $weight) {
        try {
            parent::__construct($sku, $name, $price);
            $this->setWeight($weight);
        } catch (Exception $ex) {
            require_once 'view/Error/Error2.php';
        }
    }

    /*     * <p>Function used to set the weight of the Book</p>
      @param int $weight <p>Weight is in kg</p>
      @Exception <p>if the weight is less then 0 a exception will produce</p> */

    function setWeight($weight) {
        if ($weight > 0) {
            $this->weight = $weight;
        } else {
            throw new Exception();
        }
    }

    /*     * <p>Function used to get the weight of a book</p>
      @return int <p>Weight of the book</p> */

    function getWeight() {
        return $this->weight;
    }

    /*     * <p>Function insert which is used to insert data into the database</p>
      @param object $Book <p>$Book is a object getting it as a parameter</p> */

    function insert(Book $book) {
        try {
            $sql = "INSERT INTO product (sku,name,price) VALUES (?, ?, ?)";
            $this->getPdo()->prepare($sql)->execute(array($book->getSku(), $book->getName(), $book->getPrice()));

            $sql2 = "INSERT INTO book (sku,weight) VALUES (?, ?)";
            $this->getPdo()->prepare($sql2)->execute(array($book->getSku(), $book->getWeight()));
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    /*     * <p>Function List_Book which is used to show data from the database</p>
      @param object $Book <p>$Book is a object getting it as a parameter</p> */

    static function List_Book() {
        try {
            $result = array();
            $db = Database::StartUp();
            $stm = $db->prepare("SELECT * FROM product p JOIN book b ON p.sku = b.sku");
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

}
?>

