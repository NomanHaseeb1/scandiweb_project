<?php

include_once 'database.php';

class Product {
    /* Atributes */
    private $pdo;
    private $sku;
    private $name;
    private $price;

    /* <p>Constructor used to connect to the database and create a object Product</p> */

    public function __construct($sku, $name, $price) {
        try {
            $this->pdo = Database::StartUp();
            $this->setSku($sku);
            $this->setName($name);
            $this->setPrice($price);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    /*     * <p>Function used to set the sku of a Product.</p>
      @param string $sku <p>Sku</p>
      @Exception <p>It shoud be a string and cann't be null</p> */

    function setSku($sku) {
        if (is_string($sku) != null) {  
            $this->sku = $sku;
        } else {
            throw new Exception();
        }
    }

    /*     * <p>Function used to set the name of a Product.</p>
      @param string $name <p>Name of the product</p>
      @Exception <p>It shoud be a string and cann't be null</p> */

    function setName($name) {
        if (is_string($name) != null) {
            $this->name = $name;
        } else {
            throw new Exception();
        }
    }

    /*     * <p>Function used to set the price of a Product.</p>
      @param float $price <p>Sku</p>
      @Exception <p>It should be greater than 0.01</p> */

    function setPrice($price) {
        if ($price > 0.01) {
            $this->price = $price;
        } else {
            throw new Exception();
        }
    }

    /*     * <p>Function used to get the sku of a Product.</p>
      @return string $sku  <p>Sku of the product</p> */

    function getSku() {
        return $this->sku;
    }

    /*     * <p>Function used to get the name of a Product.</p>
      @return string $name  <p>name of the product</p> */

    function getName() {
        return $this->name;
    }

    /*     * <p>Function used to get the price of a Product.</p>
      @return float $price  <p>price of the product</p> */

    function getPrice() {
        return $this->price;
    }

    /*     * <p>Function used to get the pdo object</p>
      @return object $pdo  <p>PDO</p> */

    function getPdo() {
        return $this->pdo;
    }

    /*     * <p>Function used to check if the product exists in the table using sku of the product.</p>
     * @param string $sku <p>Sku will be passed via paramter.</p>
      @return boolean   <p>if the product exists it will return false
     *               if the product doesn't exists it will return true.</p> */

    public function checkSku($sku) {
        $sql = "SELECT count(*) AS counter FROM product WHERE sku = ?";
        $stmt = $this->getPdo()->prepare($sql);
        $stmt->bindValue(1, $sku);
        $stmt->execute();
        $counts = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($counts['counter'] > 0) {
            return false;
        } else {
            return true;
        }
    }

    /*     * <p>Function used to delete all the products which are selected in the show.php view.</p>
     * @param array $products <p>Products are passed throw the parameter.</p> */

    static function deleteProductChecked($products) {
        foreach ($products as $value) {
            $sql = 'DELETE FROM product '
                    . 'WHERE sku = :sku';
            $stmt = $this->getPdo()->prepare($sql);
            $stmt->bindValue(':sku', $value);
            $stmt->execute();
        }
    }

}
