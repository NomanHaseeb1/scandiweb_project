<?php
class Database
{
    /*Static function used to connect with the database*/
    public static function StartUp()
    {
        $pdo = new PDO('mysql:host=localhost;dbname=scandiweb;charset=utf8', 'root', '');
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);	
        return $pdo;
    }
}