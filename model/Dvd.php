<?php

require_once 'Product.php';

class Dvd extends Product {
    /* Atributes */

    private $size;

    /*     * <p>Constructor used to create a Dvd.</p> */

    public function __construct($sku, $name, $price, $size) {
        try {
            parent::__construct($sku, $name, $price);
            $this->setSize($size);
        } catch (Exception $exc) {
            require_once 'view/Error/Error2.php';
        }
    }

    /*     * <p>Function used to set the size of the Dvd.</p>
      @param int $size <p>size is in mb.</p>
      @Exception <p>if the size is less then 0 a exception will produce.</p> */

    function setSize($size) {
        if ($size > 0) {
            $this->size = $size;
        } else {
            throw new Exception();
        }
    }

    /*     * <p>Function used to get the size of the Dvd.</p>
      @param int $size <p>size is in mb.</p> */

    function getSize() {
        return $this->size;
    }

    /*     * <p>Function used to insert a Dvd in a database.</p>
      @param object $dvd <p>Dvd is a object</p>
      @Exception <p>If their is a Error while connecting to the database</p> */

    function insert(Dvd $dvd) {
        try {
            $sql = "INSERT INTO product (sku,name,price) VALUES (?, ?, ?)";
            $this->getPdo()->prepare($sql)->execute(array($dvd->getSku(), $dvd->getName(), $dvd->getPrice(),));

            $sql2 = "INSERT INTO dvd (sku,size) VALUES (?, ?)";
            $this->getPdo()->prepare($sql2)->execute(array($dvd->getSku(), $dvd->getSize()));
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    /*     * <p>Function used to list all the dvd from the database.</p>
      @return $stm using pdo fetech object method */

    static function List_Dvd() {
        try {
            $result = array();
            $db= Database::StartUp();
            $stm = $db->prepare("SELECT * FROM product p JOIN dvd d ON p.sku = d.sku");
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

}

?>
