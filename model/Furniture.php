<?php

require_once 'Product.php';

class Furniture extends Product {
    /* Atributes */

    private $hieght;
    private $width;
    private $length;

    /*     * <p>Constructor used to create a Dvd.</p> */

    public function __construct($sku, $name, $price, $hieght, $width, $length) {
        try {
            parent::__construct($sku, $name, $price);
            $this->setHieght($hieght);
            $this->setWidth($width);
            $this->setLength($length)   ;
        } catch (Exception $e) {
            require_once 'view/Error/Error2.php';
        }
    }

    /*     * <p>Function used to set the hiegth of a Furniture.</p>
      @param int $hieght <p>Hieght</p>
      @Exception <p>if the hieght is less then 0 a exception will produce.</p> */

    function setHieght($hieght) {
        if ($hieght > 0) {
            $this->hieght = $hieght;
        } else {
            throw new Exception();
        }
    }

    /*     * <p>Function used to set the width of a Furniture.</p>
      @param int $width <p>Width</p>
      @Exception <p>if the width is less then 0 a exception will produce.</p> */

    function setWidth($width) {
        if ($width > 0) {
            $this->width = $width;
        } else {
            throw new Exception();
        }
    }

    /*     * <p>Function used to set the length of a Furniture.</p>
      @param int $length <p>Length</p>
      @Exception <p>if the length is less then 0 a exception will produce.</p> */

    function setLength($length) {
        if ($length > 0) {
            $this->length = $length;
        } else {
            throw new Exception();
        }
    }

    /*     * <p>Function used to get the hiegth of a Furniture.</p>
      @return int $hieght */

    function getHieght() {
        return $this->hieght;
    }

    /*     * <p>Function used to get the width of a Furniture.</p>
      @return int $width */

    function getWidth() {
        return $this->width;
    }

    /*     * <p>Function used to get the length of a Furniture.</p>
      @return int $length */

    function getLength() {
        return $this->length;
    }

    /*     * <p>Function used to insert a Furniture in a database.</p>
      @param Furniture $data <p>Furniture object</p> */

    public function insert(Furniture $data) {
        try {
            $sql = "INSERT INTO product (sku,name,price) VALUES (?, ?, ?)";
            $this->getPdo()->prepare($sql)->execute(array($data->getSku(), $data->getName(), $data->getPrice(),));

            $sql2 = "INSERT INTO furniture (sku,hieght,width,length) VALUES (?, ?, ?, ?)";
            $this->getPdo()->prepare($sql2)->execute(array($data->getSku(), $data->getHieght(), $data->getWidth(), $data->getLength(),));
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    /*     * <p>Function used to list the furnitures from the database.</p>
      @return $stm <p>returns furniture using pdo.</p> */

    static function List_Furnitures() {
        try {
            $result = array();
            $dbcon= Database::StartUp();
            $stm = $dbcon->prepare("SELECT * FROM product p JOIN furniture f ON p.sku = f.sku");
            $stm->execute();
            return $stm->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

}
